#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 24 20:50:27 2021
Organizing code in classes
@author: wneal
"""
from random import * #This includes the choice method.
#This is a class into which the maze game code is to be organized
class mazeGame:
    #This is a function that initializes the game:  
    #This is a constructor - this function runs automatically when the class is invoked.
    def __init__(self):
        self.maze = [[1,1,0,0,1,1,1],
        [1,0,0,0,0,1,1],
        [1,0,1,0,1,0,0],
        [1,0,1,0,1,0,1],
        [1,0,1,1,1,0,1],
        [1,0,0,0,0,0,1],
        [1,1,1,1,1,1,1]
        ]
        self.rooms = [["Entrance","","Back Yard"],
                      ["Living Room","","Porch"],
                      ["Kitchen","Staircase","Hallway"]]
        self.items = ["sword","pen","calculator","frog","car"]
        self.itemlocations = [[0,0],[0,2],[2,1],[1,2],[1,0]]
        self.row = 1
        self.column = 3
        self.alive = True #Is the character still alive?
        self.directionList = []
        
    #This function may be used for displaying introductory information.    
    def introduction(self):
        print("Welcome to the 'wander through the house' game!")
        #Provide  information about how to play the game, scenario of the game,  etc.
    
    #This function is used to display the directions in which the player may move.
    def directions(self):
        self.directionList = []
        print("You may travel in the following directions...")
        if self.maze[self.row][self.column+1]==0:
            print("East")
            self.directionList.append("East")
        if self.maze[self.row][self.column-1]==0:
            print("West")
            self.directionList.append("West")
        if self.maze[self.row+1][self.column]==0:
            print("South")
            self.directionList.append("South")
        if self.maze[self.row-1][self.column]==0:
            print("North")
            self.directionList.append("North")
            
        #And so on for other directions
        
    def takeInput(self):
        print("What would you like to do?")
        #Take input here...take candle
        #command = input("Please enter a command")
        command = choice(self.directionList) #This is a built-in string method
        #Process the command here....
        self.wordList = ["take","candle"]
        print(command)
    
    def move(self):
        print("We are moving...")
        #Additional code here to test if the move is valid and to update the player's location
    
    def get(self):
        #These three variables can only be used inside this function.
        itemIndex = self.items.index(self.object) #Find the index of the item in the items list
        itemLocation = self.itemLocations[itemIndex] #This is a list representing the item's location
        currentLocation = [self.row,self.column] #This is a list representing the current location
        if self.object in self.items:
            if itemLocation == currentLocation:
                self.itemLocations[itemIndex] = [-1,-1]  #This is a code for holding the item
                print("You have just picked up the ",self.items[itemIndex])
            else:
                print("Sorry. I don't see that item here.")
        else:
            print("Huh? What is that?")
            
    
    
    def action(self):
        print("We will now act on the following command:")
        print(self.wordList)
        if len(self.wordList) >= 1:
            self.verb = self.wordList[0]
        if len(self.wordList == 2):
            self.object = self.wordList[1]
        
        if self.wordlist[0] in ["north","n","south","s","east","e","west","w","up","u","down","d"]:
            self.move()
            
        if self.wordlist[0] in ["get","take","obtain","acquire"]:
            self.get()
        #This function processes commands and changes
        #variables in the game accordingly
        #Some action may be lethal
        #In that case,
        #if.....
        #   message: oops better not run with scissors!!
        #   self.alive = False
        #After some particular action, you reach the goal:
        
        #We are cheating here: teleport the player to the end of the game!
        self.row = 0
        self.column = 2
    
    def endofGame(self):
        if self.alive == True:
            print("Congratulations!!") #Other messages, etc.
        else:
            print("Better luck next life!!!") #Other messages, etc.
            
            
    def mainLoop(self):
        self.introduction()
        while self.row!=0 or self.column!=2:
            #display directions
            self.directions()
            #take input - instructions
            self.takeInput()
            #carry out instructions
            #self.action()
        self.endofGame()
    
M = mazeGame() #M is an instance of the class 'mazeGame'
M.mainLoop()