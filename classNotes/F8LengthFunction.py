#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 13:53:47 2021
Length Function - It may be used to measure the length of such items as lists, tuples, dictionaries and strings
@author: wneal
"""

from random import *
set1 = [15,42,325,235,8,234]


print(len(set1))
set1.append(45)
print(len(set1))
for i in range(25):
    set1.append(i)
    print(set1," - length - ",len(set1))
    
#This nested loop generates random words with random letters.
#The chr() function selects from the computer's ASCII
#character set. Codes 65-90 are letters of the alphabet.
for i in range(10):
    wordLength = int(20*random())
    word = ""
    for i in range(wordLength):
        word+=(chr((65+(int(26*random())))))
    print(word, " - length - ", len(word))
    
    
D = {0:"R", 1:"F", 4:"T", 6:"S"}

print(len(D))