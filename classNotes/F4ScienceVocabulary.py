#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 10:25:45 2021

@author: wneal
"""

t = open("./1000.txt","r")  # ./ -> the current directory;   "r" -> read
b = open("./Biology.txt","r")
c = open("./Chemistry.txt","r")
p = open("./Physics.txt","r")
s = open("./Science.txt","r")

biology = []
chemistry = []
physics = []
science = []
thousand = []
master = []

#Note: "\n" is a "newline" character

for line in t:
    #print(line.split("\n")[0])
    
    word = line.split("\n")[0]
    thousand.append(word)
    
#print(thousand)


for line in b:
    #print(line.split(" "))
    line = line.split(" ")
    l = []
    for item in line:
        if item != '' and item != "\n":
            l.append(item)
    biology.append(l)
#print(biology)

#The following code changes each number in each sub-list to integer format
for i in range(len(biology)):
    try: #We try the following....
        biology[i][2] = int(biology[i][2])
    except: #If it does not work, simply print a message
        print("This item did not work")
print(biology)

    
    
    
    
