#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 31 20:47:33 2021
Algorithms
Set of commands that accomplishes a given task.
@author: wneal
"""

#Sorting Algorithms - Example - built in sort method
a = [5,8,2,1,6,3,7,4]
a.sort() #The sort method is built into Python lists.
print(a)

#Sorting Algorithm:
b = [6,8]  #How do we sort a two item list?
c = [8,6]

#Sorting Algorithm:
d = [1,7,3]  #How do we sort a three item list?
e = [6,9,3]

#Sorting Algorithm:
f = [6,2,9,4]  #How do we sort a four item list?
g = [2,8,1,9]

#Sorting Algorithm:
#h = [4,7,6,......(n items)....6,2,3] #How do we sort an n-item list


#How to switch two items in Python
a = 10
b = 25
a,b = b,a #This switched a and b.
print("a: ",a, " b: ",b)





def sortList2(List):
    if List[0]>List[1]:
        List[0],List[1] = List[1],List[0]
    return List


L = [5,6]
LSorted = sortList2(L)
print(LSorted)

L = [7,2]
LSorted = sortList2(L)
print(LSorted)

