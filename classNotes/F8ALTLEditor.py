#This is a Python-based video editor
#It takes a list of mp4 files in a particular order with start and end timestamps
#and outputs a single mp4 file.



import os

cutList = [
        ["DecisionStructuresI.mp4","00:00:06", "00:03:37"],
        ["DecisionStructuresII.mp4", "00:00:06", "00:04:49"],
        ["DecisionStructuresIII.mp4","00:00:07", "00:02:35"],
        ["DecisionStructuresIV.mp4","00:00:06", "00:00:32"]
        ]
for i in range(len(cutList)):
    outputName = "z"+str(i)+".mp4"
    cutList[i].append(outputName)
print (cutList)
def cutCommand(i, s, e, o):
    #ffmpeg -i input1.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate1.ts
    command1 = "ffmpeg -i "+i+" -ss "+s+" -to "+e+" -async 1 "+o
    os.system(command1)

def cut(cutlist):
    for clip in cutlist:
        inputFile = clip[0]
        start = clip[1]
        end = clip[2]
        outputFile = clip[3]
        cutCommand(inputFile,start, end, outputFile)

def join(cutList):
    for i in range(0,len(cutList)):
        command= "ffmpeg -i "+ cutList[i][3]+"  -c copy -bsf:v h264_mp4toannexb -f mpegts i"+str(i)+".ts"
        os.system(command)
    command='ffmpeg -i "concat:'
    for i in range(0,len(cutList)):
        command+="i"+str(i)+".ts"
        if i < len(cutList)-1:
            command+='|'
    command+= '" -c copy -bsf:a aac_adtstoasc output.mp4'
    print(command)

    os.system(command)


cut(cutList)
join(cutList)
