import bpy
from math import *
from random import * 
 
n = 100
min = -10
max = 10



for i in range(n):
    # make mesh
    vertices = []
    edges = []
    faces = []

    ###############################################################################################
    #Create Vertices - 3D Coordinates - append an ordered triplet for each vertex
    ###############################################################################################
    
    
    a1 = int(random()*(max-min+1)+min)
    a2 = int(random()*(max-min+1)+min)
    a3 = int(random()*(max-min+1)+min)
    a4 = int(random()*(max-min+1)+min)
    a5 = int(random()*(max-min+1)+min)
    a6 = int(random()*(max-min+1)+min)
    a7 = int(random()*(max-min+1)+min)
    a8 = int(random()*(max-min+1)+min)
    a9 = int(random()*(max-min+1)+min)
    
    vertices.append((a1,a2,a3))
    vertices.append((a4,a5,a6))
    vertices.append((a7,a8,a9))



    ###############################################################################################
    #Create Edges - Pairings of Vertices by List Index
    ###############################################################################################

    edges.append((0,1))
    edges.append((1,2))
    edges.append((2,0))



    ###############################################################################################
    #Create Faces - Collections of 3 or more vertices (coplanar) grouped for each face by List index.
    ###############################################################################################

    faces.append((0,1,2)) #Triangle




    new_mesh = bpy.data.meshes.new('new_mesh'+str(i))
    new_mesh.from_pydata(vertices, edges, faces)
    new_mesh.update()
    # make object from mesh
    new_object = bpy.data.objects.new('new_object'+str(i), new_mesh)
    # make collection
    new_collection = bpy.data.collections.new('new_collection'+str(i))
    bpy.context.scene.collection.children.link(new_collection)
    # add object to scene collection
    new_collection.objects.link(new_object)