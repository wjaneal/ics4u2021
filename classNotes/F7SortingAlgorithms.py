#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb  7 20:56:31 2021
February 7 - Sorting Algorithms
@author: wneal
"""
from random import *

class algorithmTester:
    def __init__(self):
        #Here is a list of empty datasets
        self.set1 = []
        self.set2 = []
        self.set3 = []
        
    def createData(self):
        #In this function, we create datasets of size
        #100, 1000, 10000
        for i in range(0,100):
            self.set1.append(random())
        
    def bubbleSort(self,S):
        #This function takes a set of data, S and returns it in sorted form
        #...
        #...
        #...
        return S
    
    def insertionSort(self,S):
        #This function takes a set of data, S and returns it in sorted form
        #...
        #...
        #...
        return S
    
    def quickSort(self,S):
        #This function takes a set of data, S and returns it in sorted form
        #...
        #...
        #...
        return S
    
    def mergeSort(self,S):
        #This function takes a set of data, S and returns it in sorted form
        #...
        #...
        #...
        return S
    
    def heapSort(self,S):
        #This function takes a set of data, S and returns it in sorted form
        #...
        #...
        #...
        return S
    
    
    def main(self):
        #for each data set size
            #for each sorting algorithm
                #deepcopy the set
                #start the time using t1 = time()
                #call the algorithm
                #stop the time using t2 = time()
                #measure the difference dt = t2-t1
        #Display a table of nine sorting times: 3 datasets with three algorithms
        
A = algorithmTester()
A.main()        
        