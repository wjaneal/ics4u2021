#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 16 13:25:28 2021
Python File Processing Program - Strategy
@author: wneal
"""


#The following steps are designed to reduce the likelihood of a crash
#while processing the multiple text files in the text file processing program

#Suggested variable and file names:

#bio - name of the biology master list
#che - name of the chemistry master list
#phy - name of the physics master list
#sci - name of the science master list

#With each of the above lists, save to a text file with names as follows:
# bioproc.txt, cheproc.txt, phyproc.txt, sciproc.txt
# Write the name, part of speech (POS) and count separated by spaces and end with a newline ("\n")
# Set the bio, che, phy and sci variables to empty lists after each is saved to a file
#This will continually free up memory in the system

#Now, create four master lists:
# mstWord, mstPOS and mstCount, mstSubjectCount

# Iterate through the four processed files as follows:
#Open the file for reading
# For each line of the file, 
#   For each word in the line (word POS count) - This can be separated by spaces using the .split() method
#       If the word is not in the 1000 most common words:
#            If the word is not in the master list:        
#                Append word to the mstWord
#                Append POS to the mstPOS
#                Append Count to the mstCount
#                Append 1 to the mstSubjectCount
#             Else:
#                Find the index of the word in the master list
#                Verify that the POS at that index in mstPOS matches - raise an error if not
#                Add Count to the count at that index in mstCount
#                 Add one to the mstSubjectCount at that index

#Final processing:
#Iterate through the mstSubjectCount and append items to a new set of four lists if the mstSubectCount is three or more
# Final list names: mstWordFinal, mstPOSFinal, mstCountFinal, mstSubjectCountFinal
