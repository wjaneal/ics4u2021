#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 20:59:54 2021
#This code models an automated system that sets a parameter on a robot
to a particular value.  In this specific case, we are setting the speed
as close as possible to the given value with a motor that is either on or off.
The object has a mass, driving force and frictional force.
@author: wneal
"""

from mpl_toolkits.axisartist.axislines import SubplotZero
import matplotlib.pyplot as plt
import numpy as np




Mass = 1 #Define the mass.
forceAvailable = 100 #This is the maximum drive force available
dragForce = 20 #This is a drag coefficient
dragCoefficient = 0.1 #This is the percentage of the drag force that results in drag on the robot

targetSpeed = 100 #This is the speed we seek to achieve
lag = 0.2 #seconds 
dt = 0.01 #time increment
iterations = 2000 #How many calculations in our simulation?
count = 0 
speed = 0 #This is the initial speed
currentForce = forceAvailable #Set force to the full available force.

x = [] #Empty lists to collect x and y values we wish to plot.
y = []

#Main loop in the calculation.
while count < iterations:
    
    #Motors are 100% if below target and 0% if above.
    if speed < targetSpeed:
        currentForce = forceAvailable
    else:
        currentForce = -forceAvailable
    
    
    a = currentForce/Mass  #F = ma; so; a = F/m
    drag = dragCoefficient*dragForce
    speed+=(a-drag)*dt
    print(count, speed)
    x.append(count)
    y.append(speed)
    count+=1
    
plt.scatter(x,y)

plt.show()
