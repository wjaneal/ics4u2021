#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 20:57:50 2021
Selection Sort - Developed from Scratch

The selection sorting algorithm is as follows:
    
For each item in a data set (current location), create a variable to track
the index of the smallest item.
    For each remaining item in the data set, compare with the 
smallest and update the index of the smallest item accordingly
    Switch the smallest item with the item in the current location

@author: wneal
"""

from random import *

#Create a dataset
n = 10
dataSet = []
for i in range(n):
    dataSet.append(int(1000*random()))


def selectionSort(dS):
    #Carry out the Selection sort algorithm
    for i in range(0,len(dS)):
        smallestIndex = i
        for j in range(i+1,len(dS)):
            if dS[j]<dS[smallestIndex]:
                smallestIndex = j
        dS[i],dS[smallestIndex] = dS[smallestIndex],dS[i]
    return dS
    
    
print(dataSet)
dataSet = selectionSort(dataSet)
print(dataSet)







