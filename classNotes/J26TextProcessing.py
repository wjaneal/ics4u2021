#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 25 20:40:05 2021
Text Processing with Python String Methods
@author: wneal
"""

#This is an input statement
#It creates the string variable, 'someText'
someText = input("Please enter a sentence.")
capitals = someText.capitalize()
lowerCase = someText.lower()
upperCase = someText.upper()
titleCase = someText.title()
swapped = someText.swapcase()

print(someText)
print(capitals)
print(lowerCase)
print(upperCase)
print(titleCase)
print(swapped)

#This feature is extremely useful in processing text
words = someText.split(" ")
print(words)
print("The number of words in the sentence is: ",len(words))
print("The first word is: ",words[0])
print("The second word is: ",words[1])


if len(words)>2:
    print("That is too many words. I don't understand!!!")

verbs = ['take','get','run','observe','fight','hit','pour','drink','unlock','drop']
items = ['candle','key','shovel','calculator','monster','car','phone']

# Use the Python keyword 'in' to test if something is in something else:
if words[0] in verbs: #If the first word is in the list of verbs...
    print("You entered a verb.")
else:
    print("The first word is not a verb I recognize.")
    