#This sample Blender-Python file translates an object, adding (1,2,3) to the (x,y,z)
#coordinates each 10 frames

#This code assumes that the animation is at frame 1 to begin
import bpy
from math import *
r = 5
b = 1
for i in range(10):
    #Translate the object (the currently selected object in Blender)
    
    ##################################################################################################
    #Uncomment one of the following bpy.ops.transform.translate commands:
    
    #Parabolic Motion in y
    #bpy.ops.transform.translate(value=(1, i*i/10, 3), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
    
    #Linear Motion:
    #bpy.ops.transform.translate(value=(1, 2, 3), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
    
    #Circular Motion:
    bpy.ops.transform.translate(value=(r*cos(b*i), r*sin(b*i),0), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
    
    
    ###################################################################################################
    
    
    #Insert a keyframe to the current frame
    bpy.ops.anim.keyframe_insert_menu(type='Location')
    #Move the frame forward by 10
    bpy.context.scene.frame_set(11+10*i)
    
    