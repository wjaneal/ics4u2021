#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 15 21:00:42 2021
FILE I/O - Examples


@author: wneal
"""

from random import *

maxWordSize = 5
numWords = 30 #number of words per "line"
numLines = 1500 #number of lines of text.

fileName = "./wordFile.txt"
f = open(fileName,"w") #Open a file for writing

#This program will create a set of random words and write them to a file.
for l in range(numLines):
    line = ""
    for w in range(numWords):
        word = ""
        for i in range(int(maxWordSize*random()+1)):
            word+=chr(65+int(26*random()))
        line+=word+" "
    f.write(line) #Write the line to the file
    f.write("\n")  #Add a new line character
    f.write("\n")  #Add a new line character
f.close()
        
