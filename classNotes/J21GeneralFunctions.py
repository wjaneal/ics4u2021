#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 20:41:07 2021
A general introduction to functions
in Python
@author: wneal
"""

#Here is a simple function
#It has no arguments
#It does not return values to the function call
def simpleFunction():#def....function name...brackets()...:
    print("Hi!  This is a simple function")
    
simpleFunction() #This is known as a function call

#This function has one argment. It is sent as a variable from the function call
def oneArgument(arg1):
    print("The value of the argument sent to this function is: ",arg1)
    
oneArgument("Hello!!!!!!!!!!") #This is a function call
oneArgument(3)
oneArgument([3,5,4])
oneArgument(simpleFunction)


#Function with an argument and a return
def returnFunction(a):
    print("a:",a)
    print("The function will now return a*a")
    return a*a

#returnFunction(567)
#print(returnFunction(567))
returnedValue = returnFunction(12345)
print(returnedValue)






