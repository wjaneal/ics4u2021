#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 20:50:25 2021

@author: wneal
"""
from random import *

class mazeGame:
    def __init__(self):
        print("Welcome to the maze game!")
        self.done = False
        self.person = "A person" #The variable is first declared here.
        
    def command(self):
        print("We are executing some command!")
        print(self.person) #This variable must first be declared.
        
    def endGame(self):
        print("The game has now ended")
        
    def main(self):
        print("The main program is now executing")
        while self.done == False:
            print("We are in the loop!")
            self.command()
            if random()>0.9:
                self.done = True
        self.endGame()
                
M = mazeGame() #This executes self.__init__()
M.main()

            