#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 13:10:12 2021
Selection Sort from Scratch
@author: wneal
"""
from random import *

n = 20

set1 = []
for i in range(n):
    set1.append(int(1000*random()))
#For each element in the list,
#Scan through the rest of the list, keeping track of the smallest item
#Switch the smallest item into the current element's position

def selectionSort(L):
    for i in range(0,len(L)):
        smallestIndex = i #We are tracking the index of the smallest element
        for j in range(i,len(L)):
            if L[j] < L[smallestIndex]:
                smallestIndex = j
        L[smallestIndex],L[i] = L[i],L[smallestIndex]
        #print(i,L)
    return L        
        
print(set1)
set1 = selectionSort(set1)
print(set1)

