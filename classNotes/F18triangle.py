import bpy
from math import * 
 
r = 50 #Distance of triangle vertices from the z-axis.
h = 20 #Height of the triangular prism

# make mesh
vertices = []
edges = []
faces = []

###############################################################################################
#Create Vertices
###############################################################################################


#Bottom Layer
for i in range(3):
    vertices.append((r*cos(2*pi/3*i),r*sin(2*pi/3*i),0))    
    

#Top Layer
for i in range(3):
    vertices.append((r*cos(2*pi/3*i),r*sin(2*pi/3*i),h))    

###############################################################################################
#Create Edges
###############################################################################################

#Edges for bottom triangle
edges.append((0,1))
edges.append((1,2))
edges.append((2,0))

#Edges for top triangle
edges.append((3,4))
edges.append((4,5))
edges.append((5,3))

#Edges for sides
edges.append((0,3))
edges.append((1,4))
edges.append((2,5))

###############################################################################################
#Create Faces
###############################################################################################

faces.append((0,1,2)) #Bottom triangle
faces.append((3,4,5)) #Top triangle

#Side rectangles:
faces.append((0,1,4,3)) 
faces.append((1,2,5,4)) 
faces.append((2,0,3,5)) 


m 