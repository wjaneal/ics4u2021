#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 24 20:50:27 2021
Organizing code in classes
@author: wneal
"""

import time
import os


#This is a class into which the maze game code is to be organized
class mazeGame:
    #This is a function that initializes the game:    
    def __init__(self):
        self.maze = [[0,1,0],
                     [0,1,0],
                     [0,0,0]]
        self.rooms = [["Entrance","","Back Yard"],
                      ["Living Room","","Porch"],
                      ["Kitchen","Staircase","Hallway"]]
        
        self.row = 0
        self.column = 0
        self.alive = True #Is the character still alive?
        
    #This function may be used for displaying introductory information.    
    def introduction(self):
        print("Welcome to the 'wander through the house' game!")
        #Provide  information about how to play the game, scenario of the game,  etc.
    
    #This function is used to display the directions in which the player may move.
    def directions(self):
        print("You may travel in the following directions...")
        if self.maze[self.row][self.column+1]==0:
            print("East")
        #And so on for other directions
        
    def takeInput(self):
        print("What would you like to do?")
        #Take input here...take candle
        command = input("Please enter a command")
        #Process the command here....
        self.wordList = ["take","candle"]
    
    def action(self):
        print("We will now act on the following command:")
        print(self.wordList)
        #This function processes commands and changes
        #variables in the game accordingly
        #Some action may be lethal
        #In that case,
        #if.....
        #   message: oops better not run with scissors!!
        #   self.alive = False
        #After some particular action, you reach the goal:
        
        #We are cheating here: teleport the player to the end of the game!
        self.row = 0
        self.column = 2
    
    def endofGame(self):
        if self.alive == True:
            print("Congratulations!!") #Other messages, etc.
        else:
            print("Better luck next life!!!") #Other messages, etc.
            
            
    def mainLoop(self):
        self.introduction()
        while self.row!=0 or self.column!=2:
            #display directions
            self.directions()
            #take input - instructions
            self.takeInput()
            #carry out instructions
            self.action()
        self.endofGame()
    '''
    #########################################################################
    Modifications - add instructions, save and load functions
    #########################################################################
    '''
    def instructions(self):
        print("The instructions will be printed here")
        
    def saveResult(self):
        filePrefix = "PR"
        fileSuffix = ".txt"
        monthString = time.strftime("%B")
        dayString = time.strftime("%d")
        yearString = time.strftime("%Y")
        print(monthString,dayString,yearString)
        print("Results are now saved")
        fileName = filePrefix+"_"+monthString+"_"+dayString+"_"+yearString+fileSuffix
        f = open(fileName,"w")
        f.write("Saving the results of the game")
        f.close()
    def loadResult(self):
        path = "." #Current directory
        files = os.listdir(path)
        for f in files:
            print (f)
        print("Results are now loaded")
        fileToLoad = input("Please enter the name of a file to load")
        f = open(fileToLoad,"r")
        print("Here are the contents of the saved game:")
        for item in f:
            print(item)
        f.close()
        
    
    '''
    #########################################################################
    Modification - add menu function
    #########################################################################
    '''
    def menu(self):
        self.done = 0
        while self.done == 0:
            print("Welcome to the Maze Game!")
            print("Please select an option:")
            print("1 - Instructions")
            print("2 - Main Program")
            print("3 - Save Result")
            print("4 - Load Result")
            print("5 - Quit")
            answer = int(input("Please select (1-5)"))
            if answer == 1:
                self.instructions()
            if answer == 2:
                self.mainLoop()
            if answer == 3:
                self.saveResult()
            if answer == 4:
                self.loadResult()
            if answer == 5:
                self.done = 1
                    
        
C = mazeGame() #M is an instance of the class 'mazeGame'
C.menu()