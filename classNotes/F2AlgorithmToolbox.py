#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  1 20:48:40 2021
Toolbox for Study of Algorithms
@author: wneal
"""

#Create a random number.
from random import * #Import the random library.
print(random()) #This prints a floating point number between 0 and 1
print(10*random()) #This prints a floating point number between 0 and 10
print(int(10*random()))

for i in range(0,100):
    print(int(10*random())+1) #Integers from 1 to 10 inclusive
    
#Choose between left, right or straight forward.
for i in range(0,10):
    direction = random()
    if direction < 1/3:
        print("Left")
    elif direction > 2/3:
        print("Right")
    else:
        print("Forward")

#Create a list of random floating point numbers.
Lowest = 0
Highest = 10
n = 100
DataList = []
for i in range(0,n):
    DataList.append((Highest-Lowest)*random()+Lowest)
print(DataList)


#Create a list of random integers.
Lowest = 0
Highest = 10
n = 100
DataList = []
for i in range(0,n):
    DataList.append(int((Highest-Lowest+1)*random())+Lowest)
print(DataList)


#Create a Variable Trace

#Sorting strategy:  
#Switch the first two adjacent numbers if they are out of order
#Continue through the list with each pair of numbers
#Repeat until all numbers are in order

#Set 0
a = [4,8,1,5,2,3,7,6]
a = [4,8,1,5,2,3,7,6]
a = [4,1,8,5,2,3,7,6]
a = [4,1,5,8,2,3,7,6]
a = [4,1,5,2,8,3,7,6]
a = [4,1,5,2,3,8,7,6]
a = [4,1,5,2,3,7,8,6]
a = [4,1,5,2,3,7,6,8]

#Set 1:
a = [4,1,5,2,3,7,6,8]
a = [1,4,5,2,3,7,6,8]
a = [1,4,5,2,3,7,6,8]
a = [1,4,2,5,3,7,6,8]
a = [1,4,2,3,5,7,6,8]
a = [1,4,2,3,5,7,6,8]
a = [1,4,2,3,5,6,7,8]

#Set 2:
a = [1,4,2,3,5,6,7,8]
a = [1,4,2,3,5,6,7,8]
a = [1,2,4,3,5,6,7,8]
a = [1,2,3,4,5,6,7,8]
#.....

#Set 3

#Set 4

#Set 5

#Set 6
#By now, the 1 will have traveled across the list in the worst case
#Same with the 8.
a = [4,8,1,5,2,3,7,6]

for set in range(0,7):
    print("Set #",set)
    for pair in range(0,7):
        print(a)
        if a[pair]>a[pair+1]:
            a[pair],a[pair+1] = a[pair+1],a[pair]
    print(a)


    












