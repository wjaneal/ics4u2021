#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 20:59:54 2021
#This code models an automated system that sets a parameter on a robot
to a particular value.  In this specific case, we are setting the speed
as close as possible to the given value with a motor that is either on or off.
The object has a mass, driving force and frictional force.
@author: wneal
"""

from mpl_toolkits.axisartist.axislines import SubplotZero
import matplotlib.pyplot as plt
import numpy as np



#Define the variables in the model
#Model depends on F = ma; force equals mass times acceleration
k = 0.1 #This is a parameter that determines the adjustment to the variable force
mass = 0.1 #Define the mass.
forceAvailable = 100 #This is the maximum drive force available
dragForce = 2 #This is a drag coefficient
dragCoefficient = 1 #This is the percentage of the drag force that results in drag on the robot

targetSpeed = 100 #This is the speed we seek to achieve
lag = 0.2 #seconds 
dt = 0.01 #time increment
iterations = 2000 #How many calculations in our simulation?
count = 0 
speed = 0 #This is the initial speed
currentForce = forceAvailable #Set force to the full available force.

t = [] #Empty lists to collect x and y values we wish to plot.
v = []

#Main loop in the calculation.
while count < iterations:
    
    #Motors are 100% if below target and 0% if above.
    if speed < targetSpeed:
        currentForce = forceAvailable
    else:
        currentForce = 0
     
    #currentForce = k*(targetSpeed-speed) #The force is k times the difference of current and target speed
    
    a = currentForce/mass  #F = ma; so; a = F/m
    drag = dragCoefficient*dragForce
    speed+=(a-drag)*dt # dv = a dt #The change in speed is the accel * change in time
    print(count, speed)
    t.append(count) #Iteration (increments of 1/100 seconds)
    v.append(speed) #Speed
    count+=1
    
plt.scatter(t,v)

plt.show()
