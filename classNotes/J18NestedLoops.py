#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 12:42:05 2021
Nested Loops

Nested loops are loops inside loops.

Nested loops allow us to organize and manage
data structures in rows and columns.

This concept has numerous applications in 
computer science:
    
mathematics
games
photo processing
data storage and display
many others

@author: wneal
"""
numRows = 10
numColumns = 10
for row in range(numRows):
    for column in range(numColumns):
        print("Row: ",row, "  Column: ",column)
        

#Application 1 - 2D Lists

#Generate a 2D list:
#We use an "iterator" to do this - a type of nested loop.
l1 = [[row*column for column in range(numColumns)] for row in range(numRows)]
print(l1)
print("The length of our list is ",len(l1),".")
print("The length of the item in row 0 is ",len(l1[0]),".")
#Traverse a 2D list
for row in l1:
    for item in row:
        print(item)
       
#Traverse a 2D list - with indices
for row in range(len(l1)): #len() is the length of something in Python
    for column in range(len(l1[row])):
        print("Row", row, "  Column: ",column, " Value: ",l1[row][column])
      
columnWidth = 7 #Define the width of the columns to be displayed.
for row in range(len(l1)): #len() is the length of something in Python
    for column in range(len(l1[row])):
        #Typecast the numbers as strings and left justify with
        #"columnWidth" columns
        #print(l1[row][column],end = "") #Don't make a newline
        print(str(l1[row][column]).ljust(columnWidth),end="")
    print() #Print a new line    
'''

#Assignment:
    
# Make a empty "game" with a certain number of rounds and a certain number of players.
#Use a nested loop to display something like the following:
#The number of rounds and the number of players should
#depend on the variables 'numRounds' and 'numPlayers'
#Once you have set up the nested loop, include some sort of content in the game.

Round 1
Player 1, it is your turn
Player 2, it is your turn
Player 3, it is your turn
Round 2
Player 1, it is your turn
Player 2, it is your turn
Player 3, it is your turn
Round 3
Player 1, it is your turn
Player 2, it is your turn
Player 3, it is your turn
'''







