#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  1 13:34:19 2021

@author: wneal
"""

from graphics import *
win = GraphWin()

for i in range(0,50):
    pt = Point(50+i, i)
    pt.draw(win)
    
cir = Circle(pt, 25)
cir.draw(win)
cir.setOutline('red')
cir.setFill('blue')
line = Line(pt, Point(150, 100))
line.draw(win)
rect = Rectangle(Point(20, 10), pt)
rect.draw(win)