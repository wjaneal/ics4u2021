#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 15 21:00:42 2021
FILE I/O - Examples

Reading from a File.

@author: wneal
"""

fileName = "./wordFile.txt"
f = open(fileName,"r") #Open a file for reading

for line in f:
    if line =="\n":  #This line checks for "newline" characters
        print("This is a new line")
    else:
        words = line.split(" ") #This command splits the line into words using spaces as the delimiter
        for word in words:
            print(word)        
