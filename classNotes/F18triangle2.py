import bpy
from math import * 
 
r = 50 #Distance of triangle vertices from the z-axis.
h = 200 #Height of the triangular prism

# make mesh
vertices = []
edges = []
faces = []

###############################################################################################
#Create Vertices - 3D Coordinates - append an ordered triplet for each vertex
###############################################################################################
vertices.append((0,0,0))
vertices.append((1,1,0))
vertices.append((1,-1,0))



###############################################################################################
#Create Edges - Pairings of Vertices by List Index
###############################################################################################

edges.append((0,1))
edges.append((1,2))
edges.append((2,0))



###############################################################################################
#Create Faces - Collections of 3 or more vertices (coplanar) grouped for each face by List index.
###############################################################################################

faces.append((0,1,2)) #Triangle




new_mesh = bpy.data.meshes.new('new_mesh')
new_mesh.from_pydata(vertices, edges, faces)
new_mesh.update()
# make object from mesh
new_object = bpy.data.objects.new('new_object', new_mesh)
# make collection
new_collection = bpy.data.collections.new('new_collection')
bpy.context.scene.collection.children.link(new_collection)
# add object to scene collection
new_collection.objects.link(new_object)