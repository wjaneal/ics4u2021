import bpy
from math import * 
 
l = 5
h1 = 1.9
h2 = 4

# make mesh
vertices = []
edges = []
faces = []

###############################################################################################
#Create Vertices
###############################################################################################

#Bottom Vertex
vertices.append((0,0,0))

#First Layer
for i in range(5):
    vertices.append((l*cos(2*pi/5*i),l*sin(2*pi/5*i),h1))    
    

#Second Layer
for i in range(5):
    vertices.append((l*cos(2*pi/5*i+pi/5),l*sin(2*pi/5*i+pi/5),h1+h2))    

#Top Vertex
vertices.append((0,0,2*h1+h2))

###############################################################################################
#Create Edges
###############################################################################################

for i in range(1,6):
    edges.append((0,i)) #From the bottom vertex
    edges.append((11,i+5)) #From the top vertex
    edges.append((i,(i%5)+1)) #First layer
    edges.append((i+5,(i%5)+6)) #Second layer
    edges.append((i,i+5)) #Between layers 1
    edges.append((i,(i+4)%5+6)) #Between layers 2

###############################################################################################
#Create Faces
###############################################################################################

for i in range(1,6):
    faces.append((0,i,(i)%5+1)) #From the bottom vertex
    faces.append((11,i+5,(i)%5+6)) #From the top
    faces.append((i,(i)%5+1,i+5)) #Between the layers 1
    faces.append((i%5+1,i+5,(i%5)+6)) #Between the layers 2


new_mesh = bpy.data.meshes.new('new_mesh')
new_mesh.from_pydata(vertices, edges, faces)
new_mesh.update()
# make object from mesh
new_object = bpy.data.objects.new('new_object', new_mesh)
# make collection
new_collection = bpy.data.collections.new('new_collection')
bpy.context.scene.collection.children.link(new_collection)
# add object to scene collection
new_collection.objects.link(new_object)