#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 12:54:23 2021

@author: wneal
"""


maze = [[1,1,3,1,1,4],
        [1,5,6,6,1,7],
        [5,5,5,6,1,8],
        [1,1,5,1,1,9],
        [1,2,2,2,1,10],
        [1,1,1,11,11,11]]


row = 1
column = 2

roomNames = ["Nowhere","Wall","Kitchen","Living Room","Dining Room","Hallway","Bedroom", "Entrance","TV Room", "Play Room", "Closet" ]

print("You are in the ", roomNames[maze[row][column]])
