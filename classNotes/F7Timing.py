#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb  7 21:10:47 2021
The following code allows you to measure the time between two events in seconds
@author: wneal
"""

import time

t1 = time.time() #Event 1 - start the loop
for i in range(1,250):
    for j in range(1,100):
        print(i,j)
t2 = time.time() #Event 2 - end the loop
print(t1,t2,t2-t1) #The time between them is t2-t1
#time.time() is a system time measured in seconds.