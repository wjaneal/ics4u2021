# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 11:16:31 2021

@author: lenovo
"""
print("welcome to my maze game!")
import random
maze = [[1,1,1,1,1,1,1,1,1,1,1],
        [1,1,1,0,0,0,1,0,0,0,1],
        [1,0,0,0,1,0,0,0,1,1,1],
        [1,0,1,1,1,0,1,0,0,0,1],
        [1,0,1,0,0,0,1,1,1,0,1],
        [1,0,1,0,1,1,1,1,0,0,1],
        [1,1,1,0,0,0,1,1,0,1,1],
        [1,1,1,1,1,1,1,1,0,1,1]]

sRow = 5
sColumn = 1
currentrow = sRow
currentcolumn = sColumn
trow = 7
tcolumn = 8

print("Try to escape from the maze!")
for a in range(8):
         for b in range(11):
             c=maze[a][b]
             if a == currentrow and b == currentcolumn:
                 print("* ",end="")
             elif c== 1:
                 print("██",end="")
             else: 
                 print("  ",end="")
         print("")    
print()
print()

while currentrow!=trow or currentrow!=tcolumn:
    x=int(4*random.random())
    
    if x==0:
        maze[currentrow][currentcolumn]=maze[currentrow+1][currentcolumn]
        currentrow+=1
        if maze[currentrow][currentcolumn]==0:
                 print("Keep moving!")
        elif  maze[currentrow][currentcolumn]==1: 
              currentrow-=1
              print("hit the wall! Please choose another direction!!")
    
    elif x==1:
        maze[currentrow][currentcolumn]=maze[currentrow-1][currentcolumn]
        currentrow-=1
        if maze[currentrow][currentcolumn]==0:
                 print("Keep moving!")
        elif  maze[currentrow][currentcolumn]==1: 
              currentrow+=1
              print("hit the wall! Please choose another direction!!")
    
    elif x==2:
        maze[currentrow][currentcolumn]=maze[currentrow][currentcolumn+1]
        currentcolumn+=1
        if maze[currentrow][currentcolumn]==0:
                 print("Keep moving!")
        elif  maze[currentrow][currentcolumn]==1: 
              currentcolumn-=1
              print("hit the wall! Please choose another direction!!")
    
    elif x==3:
        maze[currentrow][currentcolumn]=maze[currentrow][currentcolumn-1]
        currentcolumn-=1
        if maze[currentrow][currentcolumn]==0:
                 print("Keep moving!")
        elif  maze[currentrow][currentcolumn]==1: 
              currentcolumn+=1
              print("hit the wall! Please choose another direction!!")

    for row in range(len(maze)):  
        for column in range(len(maze[row])): 
           if row==currentrow and column==currentcolumn:
               print("* ",end="")
           else:
                if maze[row][column]==1 and column!=10:
                  print("██",end="")
                elif maze[row][column]==1 and column==10:
                  print("██")
                elif maze[row][column]==0 and column!=10:
                  print("  ",end="")
    
    if currentrow==7 and currentcolumn==8:
       print()
       print("You win!!Congratulations!!")
       break       
    
